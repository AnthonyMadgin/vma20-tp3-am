﻿using PaquetDeCartes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAppJeu21.Interfaces
{
    public interface ICarte
    {
        string ToString();
        Rang Rang { get; }
        Couleur Couleur { get; }
    }
}
