using ConsoleAppJeu21.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaquetDeCartes;
using System;
using FluentAssertions;
using System.Collections.Generic;

namespace Main21Test
{
    [TestClass]
    public class Main21Test
    {
        public static IEnumerable<object[]> GetData()
        {
            yield return new object[] { Rang.DAME, Couleur.COEUR };
            yield return new object[] { Rang.NEUF, Couleur.COEUR };
            yield return new object[] { Rang.SIX, Couleur.TREFLE };
            yield return new object[] { Rang.DEUX, Couleur.CARREAU };
        }

        [TestMethod]
        [TestCategory("Piger")]
        [DynamicData(nameof(GetData), DynamicDataSourceType.Method)]
        public void SiPaquetValide_LorsquePiger_RetourneCarte(Rang rang, Couleur coul)
        {
            //Given
            Mock<IPaquet> mock = new Mock<IPaquet>();
            mock.Setup(x => x.piger()).Returns(new Carte(rang, coul));
            Main21 main = new Main21(mock.Object, 0);

            //When 
            Carte carte = main.piger();

            //Then
            carte.Rang.Should().Be(rang);
            carte.Couleur.Should().Be(coul);
        }

        [TestMethod]
        [TestCategory("Piger")]
        public void SiPaquetVide_LorsquePiger_RetourneNull()
        {
            //Given
            Mock<IPaquet> mock = new Mock<IPaquet>();
            mock.Setup(x => x.piger()).Returns((Carte) null);
            Main21 main = new Main21(mock.Object, 0);

            //When 
            //Then
            Carte carte = main.piger();
            carte.Should().BeNull();
        }

        [TestMethod]
        [TestCategory("Piger")]
        public void SiPaquetNull_LorsquePiger_RetourneNullReferenceException()
        {
            //Given
            Main21 main = new Main21(null, 0);

            //When 
            //Then
            Action pige = () => main.piger();
            pige.Should().Throw<NullReferenceException>();
        }

        [TestMethod]
        [TestCategory("NbCarte")]
        public void SiNbCarteNonNull_LorsqueAppelleConstructeur_RetourneNbCarte()
        {
            //Given
            IPaquet paquet = new Paquet(true);
            Main21 main = new Main21(paquet, 15);

            //When 
            //Then
            main.NbCarte.Should().Be(15);
        }

        [TestMethod]
        [TestCategory("NbCarte")]
        public void SiNbCarteZero_LorsqueAppelleConstructeur_RetourneNbCarte()
        {
            //Given
            IPaquet paquet = new Paquet(true);
            Main21 main = new Main21(paquet, 0);

            //When 
            //Then
            main.NbCarte.Should().Be(0);

        }

        [TestMethod]
        [TestCategory("Voir")]
        public void SiIndiceOutOfBound_LorsqueVoir_Retournenull()
        {
            //Given
            IPaquet paquet = new Paquet(true);
            Main21 main = new Main21(paquet, 0);

            //When            
            //Then
            main.voir(53).Should().BeNull();
        }

        [TestMethod]
        [TestCategory("Voir")]
        public void SiPaquetNull_LorsqueVoir_RetourneNullReferenceException()
        {
            //Given
            Main21 main = new Main21(null, 0);

            //When            
            //Then
            main.voir(3).Should().BeNull();
        }

        [DataTestMethod]        
        [TestCategory("Voir")]
        public void SiPaquetEtIndiceValide_LorsqueVoir_RetourneCarte()
        {
            //Given
            IPaquet paquet = new Paquet(false);
            Main21 main = new Main21(paquet, 2);

            //When            
            //Then
            main.voir(0).Couleur.Should().Be(Couleur.PIQUE);
            main.voir(0).Rang.Should().Be(Rang.AS);
        }

        [TestMethod]
        [TestCategory("Valeur")]
        public void SiMainValide_LorsqueAppelValeurMain21_RetourneValeur()
        {
            //Given
            IPaquet paquet = new Paquet(false);
            Main21 main = new Main21(paquet, 3);

            //When            
            //Then
            main.ValeurMainDe21.Should().Be(16);
        }

        [TestMethod]
        [TestCategory("Resultat")]
        public void SiMain21PlusPetit21_LorsqueVerifie_RetourneFaux()
        {
            //Given
            IPaquet paquet = new Paquet(false);
            Main21 main = new Main21(paquet, 1);

            //When            
            //Then
            main.main21Perdante().Should().BeFalse();
            main.main21Gagnante().Should().BeFalse();
            main.main21GagnanteOuPerdante().Should().BeFalse();
        }

        [TestMethod]
        [TestCategory("Resultat")]
        public void SiMain21PlusGrand21_LorsqueVerifie_RetourneVraiPourPerd()
        {
            //Given
            IPaquet paquet = new Paquet(false);
            Main21 main = new Main21(paquet, 10);

            //When            
            //Then
            main.main21Perdante().Should().BeTrue();
            main.main21Gagnante().Should().BeFalse();
            main.main21GagnanteOuPerdante().Should().BeTrue();
        }

        [TestMethod]
        [TestCategory("Resultat")]
        public void SiMain21egale21_LorsqueVerifie_RetourneVraiPourGagne()
        {
            //Given
            IPaquet paquet = new Paquet(true);
            Main21 main = new Main21(paquet, 2);

            //When            
            while(main.ValeurMainDe21 != 21)
            {
                if (main.ValeurMainDe21 > 21)
                {
                    main = new Main21(new Paquet(true), 2);
                }
                main.piger();
            }
            //Then
            main.main21Perdante().Should().BeFalse();
            main.main21Gagnante().Should().BeTrue();
            main.main21GagnanteOuPerdante().Should().BeTrue();
        }
    }
}
